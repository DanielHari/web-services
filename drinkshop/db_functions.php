<?php

class DB_Functions{
	
	private $conn;
	
	function __construct()
	{
		
		require_once 'db_connect.php';
		$db = new DB_Connect();
		$this->conn = $db->connect();
		
		
	}
	
	
	function __destruct()
	{
		
			//TODO: implement destruct method
		
	}
	
	
	/*
	*Check user exists
	*return true/false
	*/
	
	function checkExistsUser($phone)
	{
		
		$stmt = $this->conn->prepare("SELECT * FROM user WHERE Phone=?");
		$stmt->bind_param("s",$phone);
		$stmt->execute();
		$stmt->store_result();
		
		if($stmt->num_rows > 0)
		{
			
			$stmt->close();
			return true;
			
		}
		else{
			
			$stmt->close();
			return false;
			
		}
		
		
	}
	
	
	/*
	*Register new user
	*return User object if user was created
	*return false and show error message if have exception
	*
	*/
	public function registerNewUser($phone,$name,$birthdate,$address)
	{
		$stmt = $this->conn->prepare("INSERT INTO user(Phone,Name,Birthdate,Address) VALUES(?,?,?,?)");
		$stmt->bind_param("ssss",$phone,$name,$birthdate,$address);
		$result=$stmt->execute();
		$stmt->close();
		
		if($result)
		{
			
			$stmt=$this->conn->prepare("SELECT * FROM user WHERE Phone = ?");
			$stmt->bind_param("s",$phone);
			$stmt->execute();
			$user = $stmt->get_result()->fetch_assoc();
			$stmt->close();
			return $user;
			
		}
		else
			return false;
		
	}
	
	
	/*
	*get user information
	*return User object if user exists
	*return NULL if user not exists
	*
	*/
	
	public function getUserInformation($phone)
	{
		
		$stmt = $this->conn->prepare("SELECT * FROM user WHERE Phone=?");
		$stmt->bind_param("s",$phone);
		
		if($stmt->execute())
		{
			$user = $stmt->get_result()->fetch_assoc();
			$stmt->close();
			
			return $user;
			
			
		}
		else
			return NULL;
		
		
	}
	
	/*
	*get Banners
	*return list of Banners
	*
	*/
	
	public function getBanners()
	{
		//Select 3 newest banner
		$result = $this->conn->query("SELECT * FROM banner ORDER BY ID LIMIT 3");
		
		
		$banner = array();
		
		while($item=$result->fetch_assoc())
			$banners[] = $item;
		return $banners;
		
	}
	
	/*
	*get Menu
	*return list of Banners
	*
	*/
	
	public function getmenu()
	{
		//Select 3 newest banner
		$result = $this->conn->query("SELECT * FROM menu");
		
		
		$menu = array();
		
		while($item=$result->fetch_assoc())
			$menu[] = $item;
		return $menu;
		
	}
}


?>